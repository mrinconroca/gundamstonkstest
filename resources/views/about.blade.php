
@extends('layouts.layout')

@section('content')

    <div class="bg-light py-3">
      <div class="container">
        <div class="row">
          <div class="col-md-12 mb-0"><a href="/index">Inicio</a> <span class="mx-2 mb-0">/</span> <strong class="text-black">About</strong></div>
        </div>
      </div>
    </div>

    <div class="site-section border-bottom" data-aos="fade">
      <div class="container">
        <div class="row mb-5">
          <div class="border p-4 rounded mb-4">
            <h3 class="mb-3 h6 text-uppercase text-black d-block">Índice</h3>
            <ul class="list-unstyled mb-0">
              <li class="mb-1"><a href="#" class="d-flex"><span>Escalas</span> <span
                    class="text-black ml-auto"></span></a></li>
              <li class="mb-1"><a href="#" class="d-flex"><span>Panel Lining</span> <span
                    class="text-black ml-auto"></span></a></li>
              <li class="mb-1"><a href="#" class="d-flex"><span>Weathering</span> <span
                    class="text-black ml-auto"></span></a></li>
            </ul>
          </div>
          <div class="col-md-1"></div>
          <div class="col-md-7">

            <div class="site-section-heading pt-3 mb-4">
              <h2 class="text-black">Escalas</h2>
            </div>
            <p>Los modelos Gundam son clasificados en escalas y Grados (Grades en Inglés) en virtud de su calidad y tamaño. Las principales clasificaciones son:<br>
              > No Grade (1980): los primeros modelos que salieron en 1980.<br>
              > High Grade (1990): primera serie de calidad superior , a una escala de 1/144.<br>
              > Master Grade (1995): de calidad superior a la High Grade con mecánica interna integralmente modelada a una escala de 1/100.<br>
              > Perfect Grade (1998): una gama técnicamente más avanzada, a una escala de 1/60.<br>
              > First Grade (1999).<br>
              > High Grade Universal Century (1999): reedición de la cronología Universal Century.<br>
              > EX-model (2001): serie enfocada especialmente en vehículos y aeronaves.<br>
              > UC Hard Graph (2006): serie orientada a modelistas experimentados.<br>
              > Speed Grade (2007): robots prepintados a una escala pequeña.<br>
              > Mega Size (2009): con una escala de 1/48.<br>
              > Real Grade (2010): tienen similitudes con los Master Grade en su esqueleto interno, pero con una escala de 1/144 como los "High Grade".<br>
              La clasificación exhaustiva de los modelos es mucho más compleja, ya que dependen de la escala (que va desde 1/550 a 1/35), las mejoras (La clasificación Master Grade tiene tres versiones: la 1.5, 2.0 y 3.0) y los eventos especiales</p>

          </div>

        </div>
      </div>
    </div>

    <div class="site-section border-bottom" data-aos="fade">
      <div class="container">
        <div class="row justify-content-center mb-5">
          <div class="col-md-7 site-section-heading text-center pt-4">
            <h2>Quiénes somos?</h2>
          </div>
        </div>
        <div class="row">
          <div class="col-md-6 col-lg-3">

            <div class="block-38 text-center">
              <div class="block-38-img">
                <div class="block-38-header">
                  <img
                    src="https://d36fw6y2wq3bat.cloudfront.net/assets/images/cache/recipes/macarrones-con-bacon-y-cebolla/900/macarrones-con-bacon-y-cebolla.jpg"
                    alt="Image placeholder" class="mb-4">
                  <h3 class="block-38-heading h4">Iván Cubero</h3>
                  <p class="block-38-subheading">CEO/Co-Founder</p>
                </div>
                <div class="block-38-body">
                  <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Vitae aut minima nihil sit distinctio
                    recusandae doloribus ut fugit officia voluptate soluta. </p>
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-6 col-lg-3">
            <div class="block-38 text-center">
              <div class="block-38-img">
                <div class="block-38-header">
                  <img src="https://pbs.twimg.com/media/EFrKxADUwAEPyv_.jpg" alt="Image placeholder" class="mb-4">
                  <h3 class="block-38-heading h4">Marc Rincón</h3>
                  <p class="block-38-subheading">Co-Founder</p>
                </div>
                <div class="block-38-body">
                  <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Vitae aut minima nihil sit distinctio
                    recusandae doloribus ut fugit officia voluptate soluta. </p>
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-6 col-lg-3">
            <div class="block-38 text-center">
              <div class="block-38-img">
                <div class="block-38-header">
                  <img src="https://albotas.files.wordpress.com/2013/04/tumblr_mkyqx2m7ar1qbg80vo1_500.jpg?w=474" alt="Image placeholder" class="mb-4">
                  <h3 class="block-38-heading h4">Ismael Aveiga</h3>
                  <p class="block-38-subheading">Marketing</p>
                </div>
                <div class="block-38-body">
                  <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Vitae aut minima nihil sit distinctio
                    recusandae doloribus ut fugit officia voluptate soluta. </p>
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-6 col-lg-3">
            <div class="block-38 text-center">
              <div class="block-38-img">
                <div class="block-38-header">
                  <img src="https://i.redd.it/807dtvh591o41.jpg" alt="Image placeholder" class="mb-4">
                  <h3 class="block-38-heading h4">Gundam Stonks</h3>
                  <p class="block-38-subheading">Sales Manager</p>
                </div>
                <div class="block-38-body">
                  <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Vitae aut minima nihil sit distinctio
                    recusandae doloribus ut fugit officia voluptate soluta. </p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    </div>

    <script src="js/jquery-3.3.1.min.js"></script>
    <script src="js/jquery-ui.js"></script>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/owl.carousel.min.js"></script>
    <script src="js/jquery.magnific-popup.min.js"></script>
    <script src="js/aos.js"></script>

    <script src="js/main.js"></script>

    @endsection
