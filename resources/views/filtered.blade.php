@foreach ($figures as $figure)
              <div class="col-sm-6 col-lg-4 mb-4" data-aos="fade-up">
                <div class="block-4 text-center border">
                  <figure class="block-4-image">
                    <a href="/figures/{{$figure->id}}"><img src="{{asset($figure->img_preview)}}" alt="Image placeholder" class="img-fluid"></a>
                  </figure>
                  <div class="block-4-text p-4">
                    <h3><a href="/figures/{{$figure->id}}">{{$figure->name}}</a></h3>
                    <p class="text-primary">{{$figure->description}}</p>
                    <p class="text-primary font-weight-bold">{{$figure->price}}€</p>
                  </div>
                </div>
              </div>
            @endforeach