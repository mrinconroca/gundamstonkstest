@extends('layouts.layout')

@section('content')

      <div class="bg-light py-3">
        <div class="container">
          <div class="row">
            <div class="col-md-12 mb-0"><a href="#">Inicio</a> <span class="mx-2 mb-0">/</span><a
                href="#">Modelos</a> <span class="mx-2 mb-0">/</span> <strong class="text-black">PG Gundam
                Unicorn</strong></div>
          </div>
        </div>
      </div>

      <div class="site-section">
        <div class="container">
          <div class="row">
            <div class="col-md-1 ">
            @for ($i = 0; $i < count($imgs); $i++)
              <img src="{{asset($imgs[$i])}}"
                class="d-block img-fluid" alt="..." onclick="$('.carousel').carousel({{$i}})">
                @if($i!=count($imgs)-1)
                <br>
                @endif
            @endfor
            </div>
            <div class="col-md-3 ">
              <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
                <div class="carousel-inner">
                @for ($i = 0; $i < count($imgs); $i++)
                  @if($i==0)
                  <div class="carousel-item active">
                  @else
                  <div class="carousel-item">
                  @endif
                    <img src="{{asset($imgs[$i])}}"
                      class="d-block img-fluid" alt="...">
                  </div>
                @endfor
                </div>

              </div>
            </div>
            <div class="col-md-5">
              <h1 class="text-black">{{$figure->name}}</h1>
              <h5 class="text-black">Vendido por <a href="">Pepito</a></h5>
              <h6 class="text-black">Descripcion:</h6>
              <p>{{$figure->description}}</p>
              <h6 class="text-black">Detalles:</h6>
              <p class="border" style="padding: 10px;">Escala: <h7 class="text-black">{{$figure->scale}}</h7><br>
                Pintado:
                @if ($figure->painted)
                <svg width="2em" height="2em" viewBox="0 0 16 16" class="bi bi-check text-success"
                  fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                  <path fill-rule="evenodd"
                    d="M10.97 4.97a.75.75 0 0 1 1.071 1.05l-3.992 4.99a.75.75 0 0 1-1.08.02L4.324 8.384a.75.75 0 1 1 1.06-1.06l2.094 2.093 3.473-4.425a.236.236 0 0 1 .02-.022z" />
                </svg>
                @else
                <svg width="2em" height="2em" viewBox="0 0 16 16" class="bi bi-x text-danger"
                  fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                  <path fill-rule="evenodd"
                    d="M4.646 4.646a.5.5 0 0 1 .708 0L8 7.293l2.646-2.647a.5.5 0 0 1 .708.708L8.707 8l2.647 2.646a.5.5 0 0 1-.708.708L8 8.707l-2.646 2.647a.5.5 0 0 1-.708-.708L7.293 8 4.646 5.354a.5.5 0 0 1 0-.708z" />
                </svg>
                @endif
                <br>
                Modificado:
                @if ($figure->modified)
                <svg width="2em" height="2em" viewBox="0 0 16 16" class="bi bi-check text-success"
                  fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                  <path fill-rule="evenodd"
                    d="M10.97 4.97a.75.75 0 0 1 1.071 1.05l-3.992 4.99a.75.75 0 0 1-1.08.02L4.324 8.384a.75.75 0 1 1 1.06-1.06l2.094 2.093 3.473-4.425a.236.236 0 0 1 .02-.022z" />
                </svg>
                @else
                <svg width="2em" height="2em" viewBox="0 0 16 16" class="bi bi-x text-danger"
                  fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                  <path fill-rule="evenodd"
                    d="M4.646 4.646a.5.5 0 0 1 .708 0L8 7.293l2.646-2.647a.5.5 0 0 1 .708.708L8.707 8l2.647 2.646a.5.5 0 0 1-.708.708L8 8.707l-2.646 2.647a.5.5 0 0 1-.708-.708L7.293 8 4.646 5.354a.5.5 0 0 1 0-.708z" />
                </svg>
                @endif
                <br>
                Dañado:
                @if ($figure->damaged)
                <svg width="2em" height="2em" viewBox="0 0 16 16" class="bi bi-check text-success"
                  fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                  <path fill-rule="evenodd"
                    d="M10.97 4.97a.75.75 0 0 1 1.071 1.05l-3.992 4.99a.75.75 0 0 1-1.08.02L4.324 8.384a.75.75 0 1 1 1.06-1.06l2.094 2.093 3.473-4.425a.236.236 0 0 1 .02-.022z" />
                </svg>
                @else
                <svg width="2em" height="2em" viewBox="0 0 16 16" class="bi bi-x text-danger"
                  fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                  <path fill-rule="evenodd"
                    d="M4.646 4.646a.5.5 0 0 1 .708 0L8 7.293l2.646-2.647a.5.5 0 0 1 .708.708L8.707 8l2.647 2.646a.5.5 0 0 1-.708.708L8 8.707l-2.646 2.647a.5.5 0 0 1-.708-.708L7.293 8 4.646 5.354a.5.5 0 0 1 0-.708z" />
                </svg>
                @endif
                <br>
                Diorama:
                @if ($figure->diorama)
                <svg width="2em" height="2em" viewBox="0 0 16 16" class="bi bi-check text-success"
                  fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                  <path fill-rule="evenodd"
                    d="M10.97 4.97a.75.75 0 0 1 1.071 1.05l-3.992 4.99a.75.75 0 0 1-1.08.02L4.324 8.384a.75.75 0 1 1 1.06-1.06l2.094 2.093 3.473-4.425a.236.236 0 0 1 .02-.022z" />
                </svg>
                @else
                <svg width="2em" height="2em" viewBox="0 0 16 16" class="bi bi-x text-danger"
                  fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                  <path fill-rule="evenodd"
                    d="M4.646 4.646a.5.5 0 0 1 .708 0L8 7.293l2.646-2.647a.5.5 0 0 1 .708.708L8.707 8l2.647 2.646a.5.5 0 0 1-.708.708L8 8.707l-2.646 2.647a.5.5 0 0 1-.708-.708L7.293 8 4.646 5.354a.5.5 0 0 1 0-.708z" />
                </svg>
                @endif
              </p>

            </div>
            <div class="col-md-3">
              <div class="border" style="padding: 30px;">
                <p><strong class="text-primary h4">${{$figure->price}}</strong></p>
                <p><a href="/cart/add/{{$figure->id}}" class="buy-now btn btn-sm btn-primary">Añadir al carro</a></p>
                <p><a href="#" class="buy-now btn btn-sm btn-primary">Comprar ya</a></p>
              </div>
            </div>
          </div>
        </div>
      </div>

      <footer class="site-footer border-top">
        <div class="row pt-5 mt-5 text-center">
          <div class="col-md-12">
            <p>
              <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
              Copyright &copy;
              <script data-cfasync="false"
                src="/cdn-cgi/scripts/5c5dd728/cloudflare-static/email-decode.min.js"></script>
              <script>document.write(new Date().getFullYear());</script> All rights reserved | This template is made
              with <i class="icon-heart" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank"
                class="text-primary">Colorlib</a>
              <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
            </p>
          </div>

        </div>
    </div>
    </footer>
    </div>

  @endsection
