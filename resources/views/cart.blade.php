
@extends('layouts.layout')

@section('content')

    <div class="bg-light py-3">
      <div class="container">
        <div class="row">
          <div class="col-md-12 mb-0"><a href="index.html">Home</a> <span class="mx-2 mb-0">/</span> <strong
              class="text-black">Carrito</strong></div>
        </div>
      </div>
    </div>

    <div class="site-section">
      <div class="container">
        <div class="row mb-5">
          <form class="col-md-12" method="post">
            <div class="site-blocks-table">
              <table class="table table-bordered">
                <thead>
                  <tr>
                    <th class="product-thumbnail">Imagen</th>
                    <th class="product-name">Modelo</th>
                    <th class="product-price">Precio</th>
                    <th class="product-remove">Eliminar</th>
                  </tr>
                </thead>
                <tbody>
                @for ($i = 0; $i < count($carrito); $i++)
                  <tr>
                    <td class="product-thumbnail">
                    <a href="/figures/{{$carrito[$i]['id']}}"><img src="{{asset($carrito[$i]['img_preview'])}}"
                        alt="Image" class="img-fluid"></a>
                    </td>
                    <td class="product-name">
                    <a href="/figures/{{$carrito[$i]['id']}}"><h2 class="h5 text-black">{{$carrito[$i]['name']}}</h2></a>
                    </td>
                    <td>{{$carrito[$i]['price']}}€</td>
                    <td><a href="/cart/remove/{{$i}}" class="btn btn-primary btn-sm">X</a></td>
                  </tr>
                  @endfor
                </tbody>
              </table>
            </div>
          </form>
        </div>

        <div class="row">
          <div class="col-md-6">
            <div class="row mb-5">
              <div class="col-md-6">
                <button class="btn btn-outline-primary btn-sm btn-block"
                  onclick="window.location='/figures'">Continuar Comprando</button>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12">
                <label class="text-black h4" for="coupon">Cupón de descuento</label>
                <p>Inserte aquí su cupón.</p>
              </div>
              <div class="col-md-8 mb-3 mb-md-0">
                <input type="text" class="form-control py-3" id="coupon" placeholder="Código Cupón">
              </div>
              <div class="col-md-4">
                <button class="btn btn-primary btn-sm">Utilizar cupón</button>
              </div>
            </div>
          </div>
          <div class="col-md-6 pl-5">
            <div class="row justify-content-end">
              <div class="col-md-7">
                <div class="row">
                  <div class="col-md-12 text-right border-bottom mb-5">
                    <h3 class="text-black h4 text-uppercase">Total carrito</h3>
                  </div>
                </div>
                <div class="row mb-3">
                  <div class="col-md-6">
                    <span class="text-black">Subtotal</span>
                  </div>
                  <div class="col-md-6 text-right">
                    <strong class="text-black">$230.00</strong>
                  </div>
                </div>
                <div class="row mb-5">
                  <div class="col-md-6">
                    <span class="text-black">Total</span>
                  </div>
                  <div class="col-md-6 text-right">
                    <strong class="text-black">$230.00</strong>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-12">
                    <button class="btn btn-primary btn-lg py-3 btn-block"
                      >Confirmar Compra</button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  @endsection
