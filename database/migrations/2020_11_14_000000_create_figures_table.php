<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFiguresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('figures', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('img');
            $table->string('img_preview');
            $table->date('created_at');
            $table->date('updated_at');
            $table->text('description');
            $table->boolean('painted');
            $table->boolean('modified');
            $table->boolean('damaged');
            $table->boolean('diorama');
            $table->string('scale');
            $table->double('price', 8, 2);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('figures');
    }
}