<?php

namespace App\Exceptions;

use Exception;

class FileFormatException extends Exception 
{
    public function customMessage(){
        return 'Uno de los archivos no tiene un formato valido(JPG, JPGE o PNG)';
    }
}
